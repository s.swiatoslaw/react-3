import Product from '../component/Product/Product'
import {useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { fetchCards, addCart } from '../store/actions';
export default function ProductPage() {
  const products = useSelector(state => state.products);
  const cart = useSelector(state => state.cart);
  const favorite = useSelector(state => state.favorite);

  const dispatch = useDispatch();

  useEffect(() => {
      if (localStorage.getItem('cart')) {
        dispatch(addCart(JSON.parse(localStorage.getItem('cart'))))
      } 
      fetchCards();
  })
    const addToCart = (value) => {
      dispatch(addCart([...cart, value]))
      localStorage.setItem('cart', JSON.stringify(cart));
      }

      const addToFavorite = (value) => {
        // setFavorite([...favorite, value]);
        localStorage.setItem('favorite', JSON.stringify(favorite));
        }
  
  
    return (
        <div className="product-list">
        {products.map((item, key) => {
            return (
              <>
                <Product
                  id={key}
                  name={item.name}
                  price={item.price}
                  image={item.image}
                  article={item.article}
                  sendCart={addToCart}
                  sendFavorite={addToFavorite}
                />
              </>
            )
          } 
        )}
        </div>

    )
}