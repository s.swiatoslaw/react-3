import Product from '../component/Product/Product'
import getProducts from '../api/getProducts'
import {useEffect, useState} from 'react'

export default function FavoritePage() {
    const [products, setProducts] = useState([]);
    const [favorite, setFavorite] = useState([]);

    useEffect(() => {
        if (localStorage.getItem('favorite')) {
          setFavorite(JSON.parse(localStorage.getItem('favorite')))
        }
        getProducts().then(response => {
            setProducts(response.data)
        })      
    },[])

    const removeFavorite = (value) => {
        console.log("old" + favorite)
        const id = favorite.indexOf(value)
        if(id > -1) {
            const test = setFavorite(favorite.splice(id, 1))
            console.log(test)
            localStorage.setItem('favorite', JSON.stringify(favorite));
        }
        console.log("new" + favorite)
    }

    return (
       <div className="cart-list">
            {
                favorite.map((elem) => {
                    const filterFavorite = products.filter((e) => {
                        return e.article == elem;           
                    })
                    const favorite = filterFavorite.map((item, key) => {
                        return (
                        <>
                            <Product
                            key={key}
                            name={item.name}
                            price={item.price}
                            image={item.image}
                            article={item.article}
                            sendCart={removeFavorite}
                            isFavorit={true}
                            />
                        </>
                        )
                    })
                return favorite
            })
        
            }
       </div> 
    )
}