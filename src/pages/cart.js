import Product from '../component/Product/Product'
import {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { fetchCards, addCart } from '../store/actions';
export default function CartPage() {
    const products = useSelector(state => state.products);
    const cart = useSelector(state => state.cart);
    
    const dispatch = useDispatch();

    useEffect(() => {
        if (localStorage.getItem('cart')) {
          dispatch(addCart(JSON.parse(localStorage.getItem('cart'))))
        } 
        dispatch(fetchCards());
    },[])

    const removeCart = (value) => {
        const id = cart.indexOf(value)
        if(id > -1) {
            let editCart = cart.splice(id, 1);
            // setCart(cart.splice(id, 1))
            localStorage.setItem('cart', JSON.stringify(cart));
        }
    }
    return (
       <div className="cart-list">
            {
                cart.map((elem) => {
                    const filterProducts = products.filter((e) => {
                        return e.article == elem;           
                    })
                    const product = filterProducts.map((item, key) => {
                        return ( 
                        <>
                            <Product
                            key={key}
                            name={item.name}
                            price={item.price}
                            image={item.image}
                            article={item.article}
                            sendId={removeCart}
                            isCart={true}
                            />
                        </>
                        )
                    })
                return product
            })
        
            }
       </div> 
    )
}