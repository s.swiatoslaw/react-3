import './App.scss';
import Header from './component/Header/header'
import {Route, Switch} from 'react-router-dom'
import ProductPage from './pages/product'
import CartPage from './pages/cart'
import FavoritePage from './pages/favorite'

function App() {
    return (
      <>
      <Header/>
      <Switch>
      <Route exact path="/">
          <ProductPage/>
        </Route>
        <Route path="/cart">
          <CartPage/>
        </Route>
        <Route path="/favorite">
          <FavoritePage/>
        </Route>
      </Switch>
      </>
    )
}

export default App;
